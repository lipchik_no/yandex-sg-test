#!/usr/bin/env bash
HOSTS=$@
echo "Check $(hostname)"
for HOST in $HOSTS; do
  ANSWER_CODE=$(curl -s -o /dev/null -w "%{http_code}" $HOST)
  if [ $ANSWER_CODE -eq "200" ]; then
    echo "Avaliable $HOST"
  fi
done
