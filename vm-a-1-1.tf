resource "yandex_compute_instance" "vm-test-a-1-1" {
  name        = "vm-test-a-1-1"
  platform_id = "standard-v1"
  zone        = "ru-central1-a"
  hostname    = "vm-test-a-1-1"

  resources {
    cores  = 2
    memory = 2
  }

  boot_disk {
    initialize_params {
      image_id = "fd8urjqioepver7sq82o" # ubuntu-20-04-lts-v20210818
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.test-a-1.id
    ipv6 = false
    nat  = true
  }

  metadata = {
    ssh-keys = "ubuntu:${file("~/.ssh/id_rsa.pub")}"
  }
}

resource "null_resource" "vm-test-a-1-1-provision" {
  provisioner "local-exec" {
    command = format("sleep 60; ansible all -i %s, -u ubuntu -b -m shell -a 'apt update -y && apt install nginx -y'",
      yandex_compute_instance.vm-test-a-1-1.network_interface[0].nat_ip_address)
    environment = {
      ANSIBLE_HOST_KEY_CHECKING = "False"
    }
  }
  depends_on = [yandex_compute_instance.vm-test-a-1-1]
}

