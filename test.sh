#!/usr/bin/env bash
HOSTS=$@
for HOST in $HOSTS; do
  scp ./check.sh ubuntu@$HOST:/tmp/check.sh
  ssh ubuntu@$HOST /tmp/check.sh $HOSTS
done