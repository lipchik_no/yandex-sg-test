resource "yandex_vpc_network" "test-a" {
  name = "test-a"
}

resource "yandex_vpc_network" "test-b" {
  name = "test-b"
}

resource "yandex_vpc_subnet" "test-a-1" {
  v4_cidr_blocks = ["192.168.114.0/24"]
  zone           = "ru-central1-a"
  name           = "test-a-1"
  #route_table_id = ""
  network_id     = yandex_vpc_network.test-a.id
}

resource "yandex_vpc_subnet" "test-a-2" {
  v4_cidr_blocks = ["192.168.115.0/24"]
  zone           = "ru-central1-a"
  name           = "test-a-2"
  #route_table_id = ""
  network_id     = yandex_vpc_network.test-a.id
}

resource "yandex_vpc_subnet" "test-b-1" {
  v4_cidr_blocks = ["192.168.116.0/24"]
  zone           = "ru-central1-a"
  name           = "test-b-1"
  #route_table_id = ""
  network_id     = yandex_vpc_network.test-b.id
}

resource "yandex_vpc_subnet" "test-b-2" {
  v4_cidr_blocks = ["192.168.117.0/24"]
  zone           = "ru-central1-a"
  name           = "test-b-2"
  #route_table_id = ""
  network_id     = yandex_vpc_network.test-b.id
}

resource "yandex_vpc_default_security_group" "default-sg" {
  description = "description for default security group"
  network_id  = yandex_vpc_network.test-a.id

  labels = {
    my-label = "test-default"
  }

  ingress {
    protocol       = "ANY"
    description    = "Allow all from b-1"
    v4_cidr_blocks = ["192.168.116.0/24"]
    predefined_target = "loadbalancer_healthchecks"
  }
  ingress {
    protocol       = "TCP"
    description    = "Allow ssh"
    port           = 22
  }
  egress {
    protocol       = "ANY"
    description    = "Allow all from b-1"
    v4_cidr_blocks = ["192.168.116.0/24"]
    predefined_target = "loadbalancer_healthchecks"
  }
}



#output "internal_ip" {
#  value = yandex_compute_instance.mtu-test-1.*.network_interface.0.ip_address
#}
